package com.example.agendabdd.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AgendaDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA = " ,";
    private static final String SQL_CREATE_CONTACTO = " CREATE TABLE " + DefinirTabla.contacto.TABLE_NAME + "(" +
            DefinirTabla.contacto._ID + "INTEGER PRIMARY KEY, " +
            DefinirTabla.contacto.NOMBRE + TEXT_TYPE + COMMA +
            DefinirTabla.contacto.TELEFONO1 + TEXT_TYPE + COMMA +
            DefinirTabla.contacto.TELEFONO2 + TEXT_TYPE + COMMA +
            DefinirTabla.contacto.DOMICILIO + TEXT_TYPE + COMMA +
            DefinirTabla.contacto.NOTAS + TEXT_TYPE + COMMA +
            DefinirTabla.contacto.FAVORITO + INTEGER_TYPE +  ")";

    public static final String SQL_DELETE_CONTACTO = "DROP TABLE IF EXISTS " +
            DefinirTabla.contacto.TABLE_NAME;
    private static final int DATABASE_VERSION= 1;
    private static final String DATABASE_NAME= "Agenda.db";

    public AgendaDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
      db.execSQL(SQL_CREATE_CONTACTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_CONTACTO);

    }
}
